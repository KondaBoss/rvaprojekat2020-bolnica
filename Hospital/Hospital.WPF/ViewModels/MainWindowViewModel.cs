﻿using Hospital.Common.Models;
using Hospital.Common.Models.Enums;
using Hospital.WPF.Utils;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;

namespace Hospital.WPF.ViewModel
{
    public class MainWindowViewModel : INotifyPropertyChanged
    {
        #region Constructor

        public MainWindowViewModel()
        {
            LoadData();
        }

        #endregion

        #region Fields

        #region User

        private string username;
        private string password;

        #endregion

        #region Hospital

        private string hospitalName;
        private ObservableCollection<HospitalModel> hospitals;
        private HospitalModel hospitalsSelected;
        private HospitalModel hospitalsSourceSelected;
        private ObservableCollection<EHospitalType> hospitalTypes;
        private EHospitalType hospitalTypesSelected;

        #endregion

        #region Doctor

        private string doctorFirstName;
        private string doctorLastName;
        private ObservableCollection<DepartmentModel> departmentTypes;
        private DepartmentModel departmentTypesSelected;
        private ObservableCollection<EMedicalSpecialty> specialtyTypes;
        private EMedicalSpecialty specialtyTypesSelected;
        private ObservableCollection<EDoctorDegree> degreeTypes;
        private EDoctorDegree degreeTypesSelected;
        private ObservableCollection<DoctorModel> doctors;
        private DoctorModel doctorsSelected;
        private DoctorModel doctorsSourceSelected;

        #endregion

        #region Patient

        private string patientFirstName;
        private string patientLastName;
        private string patientJMBG;
        private ObservableCollection<PatientModel> patients;
        private PatientModel patientsSelected;
        private PatientModel patientsSourceSelected;

        #endregion

        #region Department

        private string nameOfDepartment;
        private ObservableCollection<DepartmentModel> departments;
        private DepartmentModel departmentsSelected;
        private DepartmentModel departmentsSourceSelected;

        #endregion

        #region Filter

        private bool filterNameChecked;
        private bool filterTypeChecked;

        #endregion

        #region Visibility

        private Visibility loginVisibility;
        private Visibility mainWindowVisibility;
        private Visibility createUserVisibility;
        private Visibility patientViewVisibility;
        private Visibility doctorViewVisibility;
        private Visibility departmentViewVisibility;

        #endregion

        #region UndoRedo

        private bool canCommandUndo;
        private bool canCommandRedo;

        #endregion

        #endregion

        #region Properties

        #region User

        public string Username
        {
            get { return username; }
            set
            {
                if (username != value)
                {
                    username = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string Password
        {
            get { return password; }
            set
            {
                if (password != value)
                {
                    password = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Hospital

        public string HospitalName
        {
            get { return hospitalName; }
            set
            {
                if (hospitalName != value)
                {
                    hospitalName = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<HospitalModel> Hospitals
        {
            get { return hospitals; }
            set
            {
                if (hospitals != value)
                {
                    hospitals = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public HospitalModel HospitalsSelected
        {
            get { return hospitalsSelected; }
            set
            {
                if (hospitalsSelected != value)
                {
                    hospitalsSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ICollectionView HospitalsSource
        {
            get
            {
                var source = new CollectionViewSource { Source = Hospitals }.View;
                source.SortDescriptions.Add(new SortDescription("ID", ListSortDirection.Ascending));
                //source.Filter = FilterHospitalSource;
                return source;
            }
        }
        public HospitalModel HospitalsSourceSelected
        {
            get { return hospitalsSourceSelected; }
            set
            {
                if (hospitalsSourceSelected != value)
                {
                    if (value != null)
                    {
                        hospitalsSourceSelected = value;
                        HospitalName = value.HospitalName;
                        HospitalTypesSelected = value.HospitalType;

                    }
                    else
                    {
                        hospitalsSourceSelected = value;
                        HospitalName = string.Empty;
                        HospitalTypesSelected = EHospitalType.Hospital;
                    }
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<EHospitalType> HospitalTypes
        {
            get { return hospitalTypes; }
            set
            {
                if (hospitalTypes != value)
                {
                    hospitalTypes = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public EHospitalType HospitalTypesSelected
        {
            get { return hospitalTypesSelected; }
            set
            {
                if (hospitalTypesSelected != value)
                {
                    hospitalTypesSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Doctor

        public string DoctorFirstName
        {
            get { return doctorFirstName; }
            set
            {
                if (doctorFirstName != value)
                {
                    doctorFirstName = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string DoctorLastName
        {
            get { return doctorLastName; }
            set
            {
                if (doctorLastName != value)
                {
                    doctorLastName = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<DepartmentModel> DepartmentTypes
        {
            get { return departmentTypes; }
            set
            {
                if (departmentTypes != value)
                {
                    departmentTypes = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public DepartmentModel DepartmentTypesSelected
        {
            get { return departmentTypesSelected; }
            set
            {
                if (departmentTypesSelected != value)
                {
                    departmentTypesSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<EMedicalSpecialty> SpecialtyTypes
        {
            get { return specialtyTypes; }
            set
            {
                if (specialtyTypes != value)
                {
                    specialtyTypes = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public EMedicalSpecialty SpecialtyTypesSelected
        {
            get { return specialtyTypesSelected; }
            set
            {
                if (specialtyTypesSelected != value)
                {
                    specialtyTypesSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<EDoctorDegree> DegreeTypes
        {
            get { return degreeTypes; }
            set
            {
                if (degreeTypes != value)
                {
                    degreeTypes = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public EDoctorDegree DegreeTypesSelected
        {
            get { return degreeTypesSelected; }
            set
            {
                if (degreeTypesSelected != value)
                {
                    degreeTypesSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<DoctorModel> Doctors
        {
            get { return doctors; }
            set
            {
                if (doctors != value)
                {
                    doctors = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public DoctorModel DoctorsSelected
        {
            get { return doctorsSelected; }
            set
            {
                if (doctorsSelected != value)
                {
                    doctorsSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ICollectionView DoctorsSource
        {
            get
            {
                var source = new CollectionViewSource { Source = Doctors }.View;
                source.SortDescriptions.Add(new SortDescription("ID", ListSortDirection.Ascending));
                //source.Filter = FilterPuteviSource;
                return source;
            }
        }
        public DoctorModel DoctorsSourceSelected
        {
            get { return doctorsSourceSelected; }
            set
            {
                if (doctorsSourceSelected != value)
                {
                    if (value != null)
                    {
                        doctorsSourceSelected = value;
                        DoctorFirstName = value.FirstName;
                        DoctorLastName = value.LastName;
                        DepartmentTypesSelected = Channel.Channel.Instance.departmentProxy.GetDepartmentByID(value.ID);
                        SpecialtyTypesSelected = value.Specialty;
                        DegreeTypesSelected = value.Degree;
                    }
                    else
                    {
                        doctorsSourceSelected = value;
                        DoctorFirstName = string.Empty;
                        DoctorLastName = string.Empty;
                        DepartmentTypes = LoadDepartmentTypes();
                        DepartmentTypesSelected = DepartmentTypes.FirstOrDefault();
                        SpecialtyTypesSelected = EMedicalSpecialty.AllergyAndImmunology;
                        DegreeTypesSelected = EDoctorDegree.AuD;
                    }
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Patient

        public string PatientFirstName
        {
            get { return patientFirstName; }
            set
            {
                if (patientFirstName != value)
                {
                    patientFirstName = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string PatientLastName
        {
            get { return patientLastName; }
            set
            {
                if (patientLastName != value)
                {
                    patientLastName = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public string PatientJMBG
        {
            get { return patientJMBG; }
            set
            {
                if (patientJMBG != value)
                {
                    if (CheckJMBG(value))
                        patientJMBG = value;
                    else
                        if (value == string.Empty)
                        patientJMBG = value;

                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<PatientModel> Patients
        {
            get { return patients; }
            set
            {
                if (patients != value)
                {
                    patients = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public PatientModel PatientsSelected
        {
            get { return patientsSelected; }
            set
            {
                if (patientsSelected != value)
                {
                    patientsSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ICollectionView PatientsSource
        {
            get
            {
                var source = new CollectionViewSource { Source = Patients }.View;
                source.SortDescriptions.Add(new SortDescription("ID", ListSortDirection.Ascending));
                //source.Filter = FilterDoctorsSource;
                return source;
            }
        }
        public PatientModel PatientsSourceSelected
        {
            get { return patientsSourceSelected; }
            set
            {
                if (patientsSourceSelected != value)
                {
                    if (value != null)
                    {
                        patientsSourceSelected = value;
                        PatientFirstName = value.FirstName;
                        PatientLastName = value.LastName;
                        PatientJMBG = value.JMBG;
                    }
                    else
                    {
                        patientsSourceSelected = value;
                        PatientFirstName = string.Empty;
                        PatientLastName = string.Empty;
                        PatientJMBG = string.Empty;
                    }

                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Department

        public string NameOfDepartment
        {
            get { return nameOfDepartment; }
            set
            {
                if (nameOfDepartment != value)
                {
                    nameOfDepartment = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ObservableCollection<DepartmentModel> Departments
        {
            get { return departments; }
            set
            {
                if (departments != value)
                {
                    departments = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public DepartmentModel DepartmentsSelected
        {
            get { return departmentsSelected; }
            set
            {
                if (departmentsSelected != value)
                {
                    departmentsSelected = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public ICollectionView DepartmentsSource
        {
            get
            {
                var source = new CollectionViewSource { Source = Departments }.View;
                source.SortDescriptions.Add(new SortDescription("ID", ListSortDirection.Ascending));
                //source.Filter = FilterDoctorsSource;
                return source;
            }
        }
        public DepartmentModel DepartmentsSourceSelected
        {
            get { return departmentsSourceSelected; }
            set
            {
                if (departmentsSourceSelected != value)
                {
                    departmentsSourceSelected = value;

                    if (value == null)
                        NameOfDepartment = string.Empty;
                    else
                        NameOfDepartment = value.DepartmentName;

                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Filter

        public bool FilterNameChecked
        {
            get { return filterNameChecked; }
            set
            {
                if (filterNameChecked != value)
                {
                    filterNameChecked = value;

                    NotifyPropertyChanged();
                }
            }
        }
        public bool FilterTypeChecked
        {
            get { return filterTypeChecked; }
            set
            {
                if (filterTypeChecked != value)
                {
                    filterTypeChecked = value;

                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region Visibility

        public Visibility LoginVisibility
        {
            get { return loginVisibility; }
            set
            {
                if (loginVisibility != value)
                {
                    loginVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public Visibility MainWindowVisibility
        {
            get { return mainWindowVisibility; }
            set
            {
                if (mainWindowVisibility != value)
                {
                    mainWindowVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public Visibility CreateUserVisibility
        {
            get { return createUserVisibility; }
            set
            {
                if (createUserVisibility != value)
                {
                    createUserVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public Visibility PatientViewVisibility
        {
            get { return patientViewVisibility; }
            set
            {
                if (patientViewVisibility != value)
                {
                    patientViewVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public Visibility DoctorViewVisibility
        {
            get { return doctorViewVisibility; }
            set
            {
                if (doctorViewVisibility != value)
                {
                    doctorViewVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }
        public Visibility DepartmentViewVisibility
        {
            get { return departmentViewVisibility; }
            set
            {
                if (departmentViewVisibility != value)
                {
                    departmentViewVisibility = value;
                    NotifyPropertyChanged();
                }
            }
        }

        #endregion

        #region UndoRedo

        public bool CanCommandUndo
        {
            get { return canCommandUndo; }
            set
            {
                if (canCommandUndo != value)
                {
                    canCommandUndo = value;

                    NotifyPropertyChanged();
                }
            }
        }
        public bool CanCommandRedo
        {
            get { return canCommandRedo; }
            set
            {
                if (canCommandRedo != value)
                {
                    canCommandRedo = value;

                    NotifyPropertyChanged();
                }
            }
        }


        #endregion

        #endregion

        #region Methods

        private void LoadData()
        {
            SetControlValues();

            LoadHospitalData();
            LoadDoctorData();
            //LoadPatientData();
            //LoadDepartmentData();

            while (true)
            {
                try
                {
                    Thread.Sleep(1000); // Waiting for server to host
                    SetHospitals();
                    break;
                }
                catch (Exception)
                {
                    Thread.Sleep(1000);
                }
            }

            #region Visibility

            LoginVisibility = Visibility.Visible;
            MainWindowVisibility = Visibility.Collapsed;
            CreateUserVisibility = Visibility.Collapsed;
            PatientViewVisibility = Visibility.Collapsed;
            DoctorViewVisibility = Visibility.Collapsed;
            departmentViewVisibility = Visibility.Collapsed;

            #endregion
        }
        private void SetControlValues()
        {
            Hospitals = new ObservableCollection<HospitalModel>();
            Departments = new ObservableCollection<DepartmentModel>();
            Doctors = new ObservableCollection<DoctorModel>();
            Patients = new ObservableCollection<PatientModel>();
        }

        #region Hospital

        private void SetHospitals()
        {
            Hospitals = new ObservableCollection<HospitalModel>(Channel.Channel.Instance.hospitalProxy.GetAll());
            NotifyPropertyChanged("Hospitals");
            NotifyPropertyChanged("HospitalsSource");
        }
        private void LoadHospitalData()
        {
            HospitalTypes = LoadHospitalTypes();
            HospitalTypesSelected = HospitalTypes.FirstOrDefault();
        }
        public ObservableCollection<EHospitalType> LoadHospitalTypes()
        {
            var collection = new ObservableCollection<EHospitalType>();
            collection.Add(EHospitalType.Hospital);
            collection.Add(EHospitalType.District);
            collection.Add(EHospitalType.Specialized);
            collection.Add(EHospitalType.Teaching);
            collection.Add(EHospitalType.Clinic);

            return collection;
        }

        #endregion

        #region Doctor

        private void SetDoctors()
        {
            if (HospitalsSourceSelected != null)
                Doctors = new ObservableCollection<DoctorModel>(Channel.Channel.Instance.doctorProxy.GetAll(HospitalsSourceSelected.ID));
            Doctors = new ObservableCollection<DoctorModel>();

            NotifyPropertyChanged("Doctors");
            NotifyPropertyChanged("DoctorsSource");
        }
        private void LoadDoctorData()
        {
            DepartmentTypes = LoadDepartmentTypes();
            DepartmentTypesSelected = DepartmentTypes.FirstOrDefault();

            SpecialtyTypes = LoadSpecialtyTypes();
            SpecialtyTypesSelected = SpecialtyTypes.FirstOrDefault();

            DegreeTypes = LoadDegreeTypes();
            DegreeTypesSelected = DegreeTypes.FirstOrDefault();

            if (HospitalsSourceSelected != null)
            {
                Doctors = new ObservableCollection<DoctorModel>(Channel.Channel.Instance.doctorProxy.GetAll(HospitalsSourceSelected.ID));
                NotifyPropertyChanged("DoctorsSource");
            }
        }
        public ObservableCollection<DepartmentModel> LoadDepartmentTypes()
        {
            if (HospitalsSourceSelected != null)
                return new ObservableCollection<DepartmentModel>(Channel.Channel.Instance.departmentProxy.GetAll(HospitalsSourceSelected.ID));
            else
                return new ObservableCollection<DepartmentModel>();
        }
        public ObservableCollection<EMedicalSpecialty> LoadSpecialtyTypes()
        {
            var collection = new ObservableCollection<EMedicalSpecialty>();
            collection.Add(EMedicalSpecialty.AllergyAndImmunology);
            collection.Add(EMedicalSpecialty.Anesthesiology);
            collection.Add(EMedicalSpecialty.Dermatology);
            collection.Add(EMedicalSpecialty.Diagnostic_Radiology);
            collection.Add(EMedicalSpecialty.Emergency_Medicine);
            collection.Add(EMedicalSpecialty.Family_Medicine);
            collection.Add(EMedicalSpecialty.Internal_Medicine);
            collection.Add(EMedicalSpecialty.Medical_Genetics);
            collection.Add(EMedicalSpecialty.Neurology);
            collection.Add(EMedicalSpecialty.Nuclear_Medicine);
            collection.Add(EMedicalSpecialty.Obstetrics_Gynecology);
            collection.Add(EMedicalSpecialty.Ophthalmology);
            collection.Add(EMedicalSpecialty.Pathology);
            collection.Add(EMedicalSpecialty.Pediatrics);
            collection.Add(EMedicalSpecialty.Physical_MedicineAndRehabilitation);
            collection.Add(EMedicalSpecialty.Preventive_Medicine);
            collection.Add(EMedicalSpecialty.Psychiatry);
            collection.Add(EMedicalSpecialty.Radiation_Oncology);
            collection.Add(EMedicalSpecialty.Surgery);
            collection.Add(EMedicalSpecialty.Urology);

            return collection;
        }
        public ObservableCollection<EDoctorDegree> LoadDegreeTypes()
        {
            var collection = new ObservableCollection<EDoctorDegree>();
            collection.Add(EDoctorDegree.AuD);
            collection.Add(EDoctorDegree.DC);
            collection.Add(EDoctorDegree.DDS);
            collection.Add(EDoctorDegree.DMD);
            collection.Add(EDoctorDegree.DO);
            collection.Add(EDoctorDegree.DPM);
            collection.Add(EDoctorDegree.DPT);
            collection.Add(EDoctorDegree.DScPT);
            collection.Add(EDoctorDegree.DSN);
            collection.Add(EDoctorDegree.DVM);
            collection.Add(EDoctorDegree.ENT);
            collection.Add(EDoctorDegree.GP);
            collection.Add(EDoctorDegree.GYN);
            collection.Add(EDoctorDegree.MD);
            collection.Add(EDoctorDegree.MS);
            collection.Add(EDoctorDegree.OB_GYN);
            collection.Add(EDoctorDegree.PharmD);

            return collection;
        }

        #endregion

        #region Patient

        private void SetPatients()
        {
            if (HospitalsSourceSelected != null)
                Patients = new ObservableCollection<PatientModel>(Channel.Channel.Instance.patientProxy.GetAll(HospitalsSourceSelected.ID));
            Patients = new ObservableCollection<PatientModel>();
            NotifyPropertyChanged("Patients");
            NotifyPropertyChanged("PatientsSource");
        }
        private void LoadPatientData()
        {
            if (HospitalsSourceSelected != null)
            {
                Patients = new ObservableCollection<PatientModel>(Channel.Channel.Instance.patientProxy.GetAll(HospitalsSourceSelected.ID));
                NotifyPropertyChanged("PatientsSource");
            }
        }
        private bool CheckJMBG(string s)
        {
            return ((s.Length <= 13) && double.TryParse(s, out double n));
        }

        #endregion

        #region Department

        private void SetDepartments()
        {
            if (HospitalsSourceSelected != null)
                Departments = new ObservableCollection<DepartmentModel>(Channel.Channel.Instance.departmentProxy.GetAll(HospitalsSourceSelected.ID));
            Departments = new ObservableCollection<DepartmentModel>();
            NotifyPropertyChanged("Departments");
            NotifyPropertyChanged("DepartmentsSource");
        }
        private void LoadDepartmentData()
        {
            if (HospitalsSourceSelected != null)
            {
                Departments = new ObservableCollection<DepartmentModel>(Channel.Channel.Instance.departmentProxy.GetAll(HospitalsSourceSelected.ID));
                NotifyPropertyChanged("DepartmentsSource");
            }
        }
        private void ResetDepartment()
        {
            DepartmentsSourceSelected = null;
            NotifyPropertyChanged("DepartmentsSource");
        }
        private void ResetAllDepartments()
        {
            Departments = new ObservableCollection<DepartmentModel>();
            ResetDepartment();
        }

        #endregion

        #region Refresh

        private void RefreshLists()
        {
            SetHospitals();
            SetDoctors();
            SetPatients();
            SetDepartments();
        }

        #endregion

        #region UndoRedo

        private void SetUndo()
        {
            CanCommandUndo = Channel.Channel.Instance.commandHandlerProxy.CanUndo();
            NotifyPropertyChanged("CanCommandUndo");
        }

        private void SetRedo()
        {
            CanCommandRedo = Channel.Channel.Instance.commandHandlerProxy.CanRedo();
            NotifyPropertyChanged("CanCommandRedo");
        }
        #endregion

        #endregion

        #region Commands

        #region Authentication

        #region Login

        public ICommand LoginCommand { get { return new RelayCommand(Login, CanLogin); } }

        private void Login(object param)
        {
            #region Visibility

            LoginVisibility = Visibility.Collapsed;
            MainWindowVisibility = Visibility.Visible;

            #endregion
        }

        private bool CanLogin(object param)
        {
            return !string.IsNullOrEmpty(Username)
                && !string.IsNullOrEmpty(Password);
        }

        #endregion

        #region Logout

        public ICommand LogoutCommand { get { return new RelayCommand(Logout, CanLogout); } }

        private void Logout(object param)
        {
            #region User

            Username = string.Empty;
            Password = string.Empty;

            #endregion

            #region Visibility

            LoginVisibility = Visibility.Visible;
            MainWindowVisibility = Visibility.Collapsed;

            #endregion
        }

        private bool CanLogout(object param)
        {
            return true;
        }

        #endregion

        #endregion

        #region Hospital

        #region Create

        public ICommand CreateHospitalCommand { get { return new RelayCommand(CreateHospital, CanCreateHospital); } }
        private void CreateHospital(object param)
        {
            Channel.Channel.Instance.hospitalProxy.CreateHospital(new HospitalModel() { HospitalName = HospitalName, HospitalType = HospitalTypesSelected });
            SetHospitals();
            SetUndo();
            SetRedo();
        }
        private bool CanCreateHospital(object param)
        {
            return !string.IsNullOrEmpty(HospitalName);
        }


        #endregion

        #region Edit

        public ICommand EditHospitalCommand { get { return new RelayCommand(EditHospital, CanEditHospital); } }
        private void EditHospital(object param)
        {
            Channel.Channel.Instance.hospitalProxy.EditHospital(HospitalsSourceSelected.ID, new HospitalModel()
            {
                ID = HospitalsSourceSelected.ID,
                HospitalName = HospitalName,
                HospitalType = HospitalTypesSelected,
                NumberOfDepartments = hospitalsSourceSelected.NumberOfDepartments,
                NumberOfDoctors = hospitalsSourceSelected.NumberOfDoctors,
                IsDeleted = false
            });
            SetHospitals();
            SetUndo();
            SetRedo();
        }
        private bool CanEditHospital(object param)
        {
            return (HospitalsSourceSelected != null)
                && !string.IsNullOrEmpty(HospitalName);
        }

        #endregion

        #region Delete

        public ICommand DeleteHospitalCommand { get { return new RelayCommand(DeleteHospital, CanDeleteHospital); } }
        private void DeleteHospital(object param)
        {
            Channel.Channel.Instance.hospitalProxy.DeleteHospital(HospitalsSourceSelected.ID);
            SetHospitals();
            SetUndo();
            SetRedo();
        }
        private bool CanDeleteHospital(object param)
        {
            return (HospitalsSourceSelected != null);
        }

        #endregion

        #region Cancel

        public ICommand CancelHospitalCommand { get { return new RelayCommand(CancelHospital, CanCancelHospital); } }
        private void CancelHospital(object param)
        {
            HospitalsSourceSelected = null;
        }
        private bool CanCancelHospital(object param)
        {
            return (HospitalsSourceSelected != null);
        }

        #endregion

        #endregion

        #region Doctor

        #region List

        public ICommand DoctorsListCommand { get { return new RelayCommand(DoctorsList, CanDoctorsList); } }
        private void DoctorsList(object param)
        {
            #region List

            LoadDoctorData();

            #endregion

            #region Visibility

            MainWindowVisibility = Visibility.Collapsed;
            DoctorViewVisibility = Visibility.Visible;

            #endregion
        }
        private bool CanDoctorsList(object param)
        {
            if (HospitalsSourceSelected == null)
                return false;

            return true;
        }

        #endregion


        #region Create

        public ICommand CreateDoctorCommand { get { return new RelayCommand(CreateDoctor, CanCreateDoctor); } }
        private void CreateDoctor(object param)
        {
            Channel.Channel.Instance.doctorProxy.CreateDoctor(new DoctorModel() { FirstName = DoctorFirstName, LastName = DoctorLastName, DepartmentID = DepartmentTypesSelected.ID, DepartmentName = DepartmentTypesSelected.DepartmentName, Degree = DegreeTypesSelected, Specialty = SpecialtyTypesSelected, IsDeleted = false });
            SetDoctors();
            SetUndo();
            SetRedo();
        }
        private bool CanCreateDoctor(object param)
        {
            return !string.IsNullOrEmpty(DoctorFirstName)
                && !string.IsNullOrEmpty(DoctorLastName);
        }


        #endregion

        #region Edit

        public ICommand EditDoctorCommand { get { return new RelayCommand(EditDoctor, CanEditDoctor); } }
        private void EditDoctor(object param)
        {
            Channel.Channel.Instance.doctorProxy.EditDoctor(DoctorsSourceSelected.ID, new DoctorModel()
            {
                ID = DoctorsSourceSelected.ID,
                FirstName = DoctorFirstName,
                LastName = DoctorLastName,
                DepartmentID = DepartmentTypesSelected.ID,
                DepartmentName = DepartmentTypesSelected.DepartmentName,
                Degree = DegreeTypesSelected,
                Specialty = SpecialtyTypesSelected,
                IsDeleted = false
            });
            SetDoctors();
            SetUndo();
            SetRedo();
        }
        private bool CanEditDoctor(object param)
        {
            return (HospitalsSourceSelected != null)
                && !string.IsNullOrEmpty(DoctorFirstName)
                && !string.IsNullOrEmpty(DoctorLastName);
        }

        #endregion

        #region Delete

        public ICommand DeleteDoctorCommand { get { return new RelayCommand(DeleteDoctor, CanDeleteDoctor); } }
        private void DeleteDoctor(object param)
        {
            Channel.Channel.Instance.doctorProxy.DeleteDoctor(DoctorsSourceSelected.ID);
            SetDoctors();
            SetUndo();
            SetRedo();
        }
        private bool CanDeleteDoctor(object param)
        {
            return (DoctorsSourceSelected != null);
        }

        #endregion


        #region Back

        public ICommand DoctorsViewBackCommand { get { return new RelayCommand(DoctorsViewBack, CanDoctorsViewBack); } }
        private void DoctorsViewBack(object param)
        {
            #region Visibility

            MainWindowVisibility = Visibility.Visible;
            DoctorViewVisibility = Visibility.Collapsed;

            #endregion
        }
        private bool CanDoctorsViewBack(object param)
        {
            return true;
        }

        #endregion

        #region Cancel

        public ICommand CancelDoctorCommand { get { return new RelayCommand(CancelDoctor, CanCancelDoctor); } }
        private void CancelDoctor(object param)
        {
            DoctorsSourceSelected = null;
        }
        private bool CanCancelDoctor(object param)
        {
            return (DoctorsSourceSelected != null);
        }

        #endregion

        #endregion

        #region Patient

        #region List

        public ICommand PatientsListCommand { get { return new RelayCommand(PatientsList, CanPatientsList); } }
        private void PatientsList(object param)
        {
            #region List

            LoadPatientData();

            #endregion

            #region Visibility

            MainWindowVisibility = Visibility.Collapsed;
            PatientViewVisibility = Visibility.Visible;

            #endregion
        }
        private bool CanPatientsList(object param)
        {
            if (HospitalsSourceSelected == null)
                return false;

            return true;
        }

        #endregion

        #region Back

        public ICommand PatientsViewBackCommand { get { return new RelayCommand(PatientsViewBack, CanPatientsViewBack); } }
        private void PatientsViewBack(object param)
        {
            MainWindowVisibility = Visibility.Visible;
            PatientViewVisibility = Visibility.Collapsed;
        }
        private bool CanPatientsViewBack(object param)
        {
            return true;
        }

        #endregion

        #region Cancel

        public ICommand CancelPatientCommand { get { return new RelayCommand(CancelPatient, CanCancelPatient); } }
        private void CancelPatient(object param)
        {
            PatientsSourceSelected = null;
        }
        private bool CanCancelPatient(object param)
        {
            return (PatientsSourceSelected != null);
        }

        #endregion

        #endregion

        #region Department

        #region List

        public ICommand DepartmentsListCommand { get { return new RelayCommand(DepartmentsList, CanDepartmentsList); } }
        private void DepartmentsList(object param)
        {
            #region List

            LoadDepartmentData();

            #endregion

            #region Visibility

            MainWindowVisibility = Visibility.Collapsed;
            DepartmentViewVisibility = Visibility.Visible;

            #endregion
        }
        private bool CanDepartmentsList(object param)
        {
            if (HospitalsSourceSelected == null)
                return false;

            return true;
        }

        #endregion

        #region Back

        public ICommand DepartmentsViewBackCommand { get { return new RelayCommand(DepartmentsViewBack, CanDepartmentsViewBack); } }
        private void DepartmentsViewBack(object param)
        {
            #region Reset

            ResetAllDepartments();

            #endregion

            #region Visibility

            MainWindowVisibility = Visibility.Visible;
            DepartmentViewVisibility = Visibility.Collapsed;

            #endregion
        }
        private bool CanDepartmentsViewBack(object param)
        {
            return true;
        }

        #endregion

        #region Cancel

        public ICommand CancelDepartmentCommand { get { return new RelayCommand(CancelDepartment, CanCancelDepartment); } }
        private void CancelDepartment(object param)
        {
            #region Reset

            ResetDepartment();

            #endregion
        }
        private bool CanCancelDepartment(object param)
        {
            return true;
        }

        #endregion

        #endregion

        #region Undo

        public ICommand UndoCommand { get { return new RelayCommand(Undo, CanUndo); } }
        private void Undo(object param)
        {
            Channel.Channel.Instance.commandHandlerProxy.Undo();
            SetUndo();
            SetRedo();
            RefreshLists();
        }
        private bool CanUndo(object param)
        {
            return CanCommandUndo;
        }

        #endregion

        #region Redo

        public ICommand RedoCommand { get { return new RelayCommand(Redo, CanRedo); } }
        private void Redo(object param)
        {
            Channel.Channel.Instance.commandHandlerProxy.Redo();
            SetRedo();
            SetUndo();
            RefreshLists();
        }
        private bool CanRedo(object param)
        {
            return CanCommandRedo;
        }

        #endregion

        #region Refresh

        public ICommand RefreshCommand { get { return new RelayCommand(Refresh, CanRefresh); } }
        private void Refresh(object param)
        {
            RefreshLists();
        }
        private bool CanRefresh(object param)
        {
            return true;
        }

        #endregion

        #endregion

        #region INotifyPropertyChanged implementation

        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged([CallerMemberName] String propertyName = "")
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #endregion
    }
}
