﻿using Common.Interfaces;
using System.ServiceModel;

namespace Hospital.WPF.Channel
{
    public class Channel
    {
        #region Fields
        public IUser userProxy;
        public IPatient patientProxy;
        public IDoctor doctorProxy;
        public IHospital hospitalProxy;
        public IDepartment departmentProxy;
        public ICommandHandler commandHandlerProxy;

        private static Channel _instance;
        #endregion

        #region Properties
        //Singleton
        public static Channel Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new Channel();
                return _instance;
            }

        }
        #endregion

        #region Constructors

        public Channel()
        {
            ChannelFactory<IUser> channelFactoryUser = new ChannelFactory<IUser>(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:4000/IUser"));
            userProxy = channelFactoryUser.CreateChannel();

            ChannelFactory<IPatient> channelFactoryPatient = new ChannelFactory<IPatient>(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:4000/IPatient"));
            patientProxy = channelFactoryPatient.CreateChannel();

            ChannelFactory<IDoctor> channelFactoryDoctor = new ChannelFactory<IDoctor>(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:4000/IDoctor"));
            doctorProxy = channelFactoryDoctor.CreateChannel();

            ChannelFactory<IHospital> channelFactoryHospital = new ChannelFactory<IHospital>(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:4000/IHospital"));
            hospitalProxy = channelFactoryHospital.CreateChannel();

            ChannelFactory<IDepartment> channelFactoryDepartment = new ChannelFactory<IDepartment>(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:4000/IDepartment"));
            departmentProxy = channelFactoryDepartment.CreateChannel();

            ChannelFactory<ICommandHandler> channelFactoryCommandHandler = new ChannelFactory<ICommandHandler>(new NetTcpBinding(), new EndpointAddress("net.tcp://localhost:4000/ICommandHandler"));
            commandHandlerProxy = channelFactoryCommandHandler.CreateChannel();
        }


        #endregion
    }
}
