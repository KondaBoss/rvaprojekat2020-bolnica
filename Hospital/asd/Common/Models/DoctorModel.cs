﻿using Hospital.Common.Models.Enums;
using Hospital.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital.Common.Models
{
    public class DoctorModel
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public DepartmentModel Department { get; set; }

        public EMedicalSpecialty Specialty { get; set; }

        public EDoctorDegree Degree { get; set; }

        // List of hospitals
    }
}
