﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital.Common.Models.Enums
{
    public enum EDoctorDegree
    {
        AuD,        // - Doctor of Audiology
        DC,         // - Doctor of Chiropractic
        DDS,        // - Doctor of Dental Surgery, Doctor of Dental Science
        DMD,        // - Doctor of Dental Medicine, Doctor of Medical Dentistry
        DO,         // - Doctor of Optometry, Doctor of Osteopathic Medicine
        DPM,        // - Doctor of Podiatric Medicine
        DPT,        // - Doctor of Physical Therapy
        DScPT,      // - Doctor of Science in Physical Therapy
        DSN,        // - Doctor of Science in Nursing
        DVM,        // - Doctor of Veterinary Medicine
        ENT,        // - Ear, nose and throat specialist
        GP,         // - General Practitioner
        GYN,        // - Gynecologist
        MD,         // - Doctor of Medicine
        MS,         // - Master of Surgery
        OB_GYN,     // - Obstetrician and Gynecologist
        PharmD,     // - Doctor of Pharmacy
    }
}
