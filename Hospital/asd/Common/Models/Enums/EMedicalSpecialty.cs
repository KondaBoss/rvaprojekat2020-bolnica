﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital.Common.Models.Enums
{
    public enum EMedicalSpecialty
    {
        AllergyAndImmunology,
        Anesthesiology,
        Dermatology,
        Diagnostic_Radiology,
        Emergency_Medicine,
        Family_Medicine,
        Internal_Medicine,
        Medical_Genetics,
        Neurology,
        Nuclear_Medicine,
        Obstetrics_Gynecology,
        Ophthalmology,
        Pathology,
        Pediatrics,
        Physical_MedicineAndRehabilitation,
        Preventive_Medicine,
        Psychiatry,
        Radiation_Oncology,
        Surgery,
        Urology
    }
}
