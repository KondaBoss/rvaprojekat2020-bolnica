﻿using Hospital.Common.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital.Common.Models
{
    public class HospitalModel
    {
        public int ID { get; set; }

        public string HospitalName { get; set; }

        public EHospitalType HospitalType { get; set; }

        public int NumberOfDoctors { get; set; }

        public int NumberOfDepartments { get; set; }

        // List of patients

        // List of doctors
    }
}
