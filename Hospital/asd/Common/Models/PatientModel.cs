﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hospital.Common.Models
{
    public class PatientModel
    {
        public int ID { get; set; }

        public string JMBG { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        // One and only one Hospital
    }
}
