﻿using Hospital.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IDoctor
    {
        [OperationContract]
        DoctorModel GetDoctor(string doctorName);

        [OperationContract]
        DoctorModel GetDoctor(int id);

        [OperationContract]
        bool CreateOrUpdateDoctor(DoctorModel doctor);
    }
}
