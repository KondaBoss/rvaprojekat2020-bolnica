﻿using Hospital.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IUser
    {
        [OperationContract]
        UserModel GetUser(string userName);

        [OperationContract]
        UserModel GetUser(int id);

        [OperationContract]
        bool CreateOrUpdateUser(UserModel user);
    }
}
