﻿using Hospital.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IDepartment
    {
        [OperationContract]
        DepartmentModel GetDepartment(string departmentName);

        [OperationContract]
        DepartmentModel GetDepartment(int id);

        [OperationContract]
        bool CreateOrUpdateDepartment(DepartmentModel department);
    }
}
