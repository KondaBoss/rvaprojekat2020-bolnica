﻿using Hospital.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IHospital
    {
        [OperationContract]
        HospitalModel GetHospital(string hospitalName);

        [OperationContract]
        HospitalModel GetHospital(int id);

        [OperationContract]
        bool CreateOrUpdateHospital(HospitalModel hospital);
    }
}
