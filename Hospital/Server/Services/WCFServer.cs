﻿using Common.Interfaces;
using Server.Command;
using System;
using System.ServiceModel;

namespace Server.Services
{
    public class WCFServer
    {
        #region Fields

        private static ServiceHost UserServiceHost;
        private static ServiceHost DoctorServiceHost;
        private static ServiceHost PatientServiceHost;
        private static ServiceHost HospitalServiceHost;
        private static ServiceHost DepartmentServiceHost;
        private static ServiceHost CommandHandlerServiceHost;

        #endregion

        #region Constructors
        public WCFServer()
        {
            UserServiceHost = new ServiceHost(typeof(UserServerProvider));
            UserServiceHost.AddServiceEndpoint(typeof(IUser), new NetTcpBinding(), new Uri("net.tcp://localhost:4000/IUser"));

            DoctorServiceHost = new ServiceHost(typeof(DoctorServerProvider));
            DoctorServiceHost.AddServiceEndpoint(typeof(IDoctor), new NetTcpBinding(), new Uri("net.tcp://localhost:4000/IDoctor"));

            PatientServiceHost = new ServiceHost(typeof(PatientServerProvider));
            PatientServiceHost.AddServiceEndpoint(typeof(IPatient), new NetTcpBinding(), new Uri("net.tcp://localhost:4000/IPatient"));

            HospitalServiceHost = new ServiceHost(typeof(HospitalServerProvider));
            HospitalServiceHost.AddServiceEndpoint(typeof(IHospital), new NetTcpBinding(), new Uri("net.tcp://localhost:4000/IHospital"));

            DepartmentServiceHost = new ServiceHost(typeof(DepartmentServerProvider));
            DepartmentServiceHost.AddServiceEndpoint(typeof(IDepartment), new NetTcpBinding(), new Uri("net.tcp://localhost:4000/IDepartment"));

            CommandHandlerServiceHost = new ServiceHost(typeof(CommandHandler));
            CommandHandlerServiceHost.AddServiceEndpoint(typeof(ICommandHandler), new NetTcpBinding(), new Uri("net.tcp://localhost:4000/ICommandHandler"));
        }

        #endregion

        #region Methods
        public void Open()
        {
            UserServiceHost.Open();
            DoctorServiceHost.Open();
            PatientServiceHost.Open();
            HospitalServiceHost.Open();
            DepartmentServiceHost.Open();
            CommandHandlerServiceHost.Open();
            Console.WriteLine($"[{DateTime.Now}]: Service opened");
        }

        public void Close()
        {
            UserServiceHost.Close();
            DoctorServiceHost.Close();
            PatientServiceHost.Close();
            HospitalServiceHost.Close();
            DepartmentServiceHost.Close();
            CommandHandlerServiceHost.Close();
            Console.WriteLine($"[{DateTime.Now}]: Service closed");
        }

        #endregion
    }
}
