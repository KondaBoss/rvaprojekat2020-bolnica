﻿using Common.Interfaces;
using Hospital.Common.Models;
using Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class PatientServerProvider : IPatient
    {
        public bool CreateOrUpdatePatient(PatientModel patient)
        {
            try
            {
                //if (!dbContext.Patients.Contains(patient))
                //{
                //    // Create
                //    dbContext.Add(patient);
                //    dbContext.SaveChanges();
                //}
                //else
                //{
                //    // Update
                //    var oldPatient = dbContext.Patients.Find(patient.ID);
                //    oldPatient = patient;
                //    dbContext.SaveChanges();
                //}
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public bool DeletePatient(int patientID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<PatientModel> GetAll(int hospitalID)
        {
            var patientHospitals = HospitalDatabase.DbContext.PatientHospitals.Where(x => !x.IsDeleted && x.HospitalID == hospitalID).ToList();
            var allPatients = HospitalDatabase.DbContext.PatientModels.Where(x => !x.IsDeleted).ToList();

            var patients = allPatients
                                    .Where(a => patientHospitals.Any(b => a.ID == b.PatientID))
                                    .Select(a => new PatientModel()
                                    {
                                        ID = a.ID,
                                        FirstName = a.FirstName,
                                        LastName = a.LastName,
                                        JMBG = a.JMBG,
                                        IsDeleted = a.IsDeleted
                                    }).ToList();

            return patients;
        }

        public PatientModel GetPatient(string jmbg)
        {
            return HospitalDatabase.DbContext.PatientModels.SingleOrDefault(x => x.JMBG == jmbg);
        }

        public PatientModel GetPatientByID(int id)
        {
            //return dbContext.Patients.SingleOrDefault(x => x.ID == id);
            throw new NotImplementedException();
        }
    }
}
