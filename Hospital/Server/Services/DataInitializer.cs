﻿using Hospital.Common.Models;
using Hospital.Common.Models.Enums;
using Server.Database;
using System.Linq;

namespace Server.Services
{
    public class DataInitializer
    {
        #region Fields

        private static DataInitializer _instance;

        #endregion

        #region Properties
        //Singleton
        public static DataInitializer Instance
        {
            get
            {
                if (_instance == null)
                    _instance = new DataInitializer();
                return _instance;
            }

        }
        #endregion

        #region Methods

        public void InitializeData()
        {
            HospitalModel hospital1 = new HospitalModel();
            HospitalModel hospital2 = new HospitalModel();

            DepartmentModel department1 = new DepartmentModel();
            DepartmentModel department2 = new DepartmentModel();
            DepartmentModel department3 = new DepartmentModel();

            #region Users

            if (!HospitalDatabase.DbContext.UserModels.Where(x => !x.IsDeleted).ToList().Any())
            {
                //Initial Users
                UserModel user1 = new UserModel()
                {
                    UserName = "admin",
                    Password = "admin",
                    FirstName = string.Empty,
                    LastName = string.Empty,
                    Role = EUserRole.Administrator,
                    IsDeleted = false
                };

                UserModel user2 = new UserModel()
                {
                    UserName = "Konda",
                    Password = "123123",
                    FirstName = "Bogdan",
                    LastName = "Kondic",
                    Role = EUserRole.Regular,
                    IsDeleted = false
                };

                HospitalDatabase.DbContext.UserModels.Add(user1);
                HospitalDatabase.DbContext.UserModels.Add(user2);

                HospitalDatabase.DbContext.SaveChanges();
            }

            #endregion

            #region Hospitals

            if (!HospitalDatabase.DbContext.HospitalModels.Where(x => !x.IsDeleted).ToList().Any())
            {
                int hospitalCnt = HospitalDatabase.DbContext.HospitalModels.ToList().Count;

                hospital1 = new HospitalModel()
                {
                    ID = ++hospitalCnt,
                    HospitalName = "Dom zdravlja",
                    HospitalType = EHospitalType.Hospital,
                    NumberOfDoctors = 0,
                    NumberOfDepartments = 0,
                    IsDeleted = false
                };

                hospital2 = new HospitalModel()
                {
                    ID = ++hospitalCnt,
                    HospitalName = "Centar za rehabilitaciju",
                    HospitalType = EHospitalType.Clinic,
                    NumberOfDoctors = 0,
                    NumberOfDepartments = 0,
                    IsDeleted = false
                };

                HospitalDatabase.DbContext.HospitalModels.Add(hospital1);
                HospitalDatabase.DbContext.HospitalModels.Add(hospital2);

                HospitalDatabase.DbContext.SaveChanges();
            }

            #endregion

            #region Patients

            if (!HospitalDatabase.DbContext.PatientModels.Where(x => !x.IsDeleted).ToList().Any())
            {
                int patientCnt = HospitalDatabase.DbContext.PatientModels.ToList().Count;
                int patientHospitalCnt = HospitalDatabase.DbContext.PatientHospitals.ToList().Count;

                // Initial Patients
                PatientModel patient1 = new PatientModel()
                {
                    ID = ++patientCnt,
                    JMBG = "1901999773630",
                    FirstName = "David",
                    LastName = "Davidovic"
                };

                PatientModel patient2 = new PatientModel()
                {
                    ID = ++patientCnt,
                    JMBG = "2905998773630",
                    FirstName = "Nikola",
                    LastName = "Bogdanovic"
                };

                HospitalDatabase.DbContext.PatientModels.Add(patient1);
                HospitalDatabase.DbContext.PatientModels.Add(patient2);

                HospitalDatabase.DbContext.SaveChanges();


                var allPatients = HospitalDatabase.DbContext.PatientModels.Where(x => !x.IsDeleted).ToList();

                // PatientHospitals

                var patientHospital1 = new PatientHospital() { ID = ++patientHospitalCnt, HospitalID = hospital1.ID, PatientID = allPatients[0].ID, IsDeleted = false };
                var patientHospital2 = new PatientHospital() { ID = ++patientHospitalCnt, HospitalID = hospital2.ID, PatientID = allPatients[1].ID, IsDeleted = false };

                HospitalDatabase.DbContext.PatientHospitals.Add(patientHospital1);
                HospitalDatabase.DbContext.PatientHospitals.Add(patientHospital2);

                HospitalDatabase.DbContext.SaveChanges();
            }

            #endregion

            #region Departments

            if (!HospitalDatabase.DbContext.DepartmentModels.Where(x => !x.IsDeleted).ToList().Any())
            {
                int departmentCnt = HospitalDatabase.DbContext.DepartmentModels.ToList().Count;
                int departmentHospitalCnt = HospitalDatabase.DbContext.DepartmentHospitals.ToList().Count;

                department1 = new DepartmentModel()
                {
                    ID = ++departmentCnt,
                    DepartmentName = "Psihijatrija",
                    IsDeleted = false
                };

                department2 = new DepartmentModel()
                {
                    ID = ++departmentCnt,
                    DepartmentName = "Hirurgija",
                    IsDeleted = false
                };

                department3 = new DepartmentModel()
                {
                    ID = ++departmentCnt,
                    DepartmentName = "Ginekologija",
                    IsDeleted = false
                };

                HospitalDatabase.DbContext.DepartmentModels.Add(department1);
                HospitalDatabase.DbContext.DepartmentModels.Add(department2);
                HospitalDatabase.DbContext.DepartmentModels.Add(department3);


                HospitalDatabase.DbContext.SaveChanges();

                var allDepartments = HospitalDatabase.DbContext.DepartmentModels.Where(x => !x.IsDeleted).ToList();


                // DepartmentHospitals
                var departmentHospital1 = new DepartmentHospital() { ID = ++departmentHospitalCnt, DepartmentID = allDepartments[0].ID, HospitalID = hospital1.ID, IsDeleted = false };
                var departmentHospital2 = new DepartmentHospital() { ID = ++departmentHospitalCnt, DepartmentID = allDepartments[1].ID, HospitalID = hospital1.ID, IsDeleted = false };
                var departmentHospital3 = new DepartmentHospital() { ID = ++departmentHospitalCnt, DepartmentID = allDepartments[2].ID, HospitalID = hospital2.ID, IsDeleted = false };

                var h1 = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == hospital1.ID);
                if (h1 != null)
                    h1.NumberOfDepartments += 2;

                var h2 = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == hospital2.ID);
                if (h2 != null)
                    h2.NumberOfDepartments++;


                HospitalDatabase.DbContext.DepartmentHospitals.Add(departmentHospital1);
                HospitalDatabase.DbContext.DepartmentHospitals.Add(departmentHospital2);
                HospitalDatabase.DbContext.DepartmentHospitals.Add(departmentHospital3);

                HospitalDatabase.DbContext.SaveChanges();
            }

            #endregion

            #region Doctors

            if (!HospitalDatabase.DbContext.DoctorModels.Where(x => !x.IsDeleted).ToList().Any())
            {
                int doctorCnt = HospitalDatabase.DbContext.DoctorModels.ToList().Count;
                int doctorHospitalCnt = HospitalDatabase.DbContext.DoctorHospitals.ToList().Count;

                //Initial Doctors
                DoctorModel doctor1 = new DoctorModel()
                {
                    ID = ++doctorCnt,
                    DepartmentID = department1.ID,
                    DepartmentName = department1.DepartmentName,
                    FirstName = "Aleksandar",
                    LastName = "Kostic",
                    Degree = EDoctorDegree.AuD,
                    Specialty = EMedicalSpecialty.Neurology,
                    IsDeleted = false
                };

                DoctorModel doctor2 = new DoctorModel()
                {
                    ID = ++doctorCnt,
                    DepartmentID = department2.ID,
                    DepartmentName = department2.DepartmentName,
                    FirstName = "Luka",
                    LastName = "Mitrovic",
                    Degree = EDoctorDegree.DC,
                    Specialty = EMedicalSpecialty.Urology,
                    IsDeleted = false
                };

                DoctorModel doctor3 = new DoctorModel()
                {
                    ID = ++doctorCnt,
                    DepartmentID = department3.ID,
                    DepartmentName = department3.DepartmentName,
                    FirstName = "Djordje",
                    LastName = "Mihailovic",
                    Degree = EDoctorDegree.GYN,
                    Specialty = EMedicalSpecialty.Psychiatry,
                    IsDeleted = false
                };

                HospitalDatabase.DbContext.DoctorModels.Add(doctor1);
                HospitalDatabase.DbContext.DoctorModels.Add(doctor2);
                HospitalDatabase.DbContext.DoctorModels.Add(doctor3);

                HospitalDatabase.DbContext.SaveChanges();

                var allDoctors = HospitalDatabase.DbContext.DoctorModels.Where(x => !x.IsDeleted).ToList();


                // DoctorHospitals
                var doctorHospital1 = new DoctorHospital() { ID = ++doctorHospitalCnt, DoctorID = allDoctors[0].ID, HospitalID = hospital1.ID };
                var doctorHospital2 = new DoctorHospital() { ID = ++doctorHospitalCnt, DoctorID = allDoctors[1].ID, HospitalID = hospital1.ID };
                var doctorHospital3 = new DoctorHospital() { ID = ++doctorHospitalCnt, DoctorID = allDoctors[2].ID, HospitalID = hospital2.ID };

                var h3 = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == hospital1.ID);
                if (h3 != null)
                    h3.NumberOfDoctors += 2;

                var h4 = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == hospital2.ID);
                if (h4 != null)
                    h4.NumberOfDoctors++;

                HospitalDatabase.DbContext.DoctorHospitals.Add(doctorHospital1);
                HospitalDatabase.DbContext.DoctorHospitals.Add(doctorHospital2);
                HospitalDatabase.DbContext.DoctorHospitals.Add(doctorHospital3);

                HospitalDatabase.DbContext.SaveChanges();
            }

            #endregion
        }

        #endregion

        #region Constructors

        public DataInitializer() { }

        #endregion
    }
}