﻿using Common.Interfaces;
using Hospital.Common.Models;
using Server.Command;
using Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class HospitalServerProvider : IHospital
    {
        #region Fields

        private static HospitalServerProvider instance;

        #endregion

        #region Properties

        public static HospitalServerProvider Instance
        {
            get
            {
                if (instance == null)
                    instance = new HospitalServerProvider();

                return instance;
            }

        }

        #endregion

        #region Constructors

        public HospitalServerProvider() { }

        #endregion

        #region Methods

        public bool CreateHospital(HospitalModel hospital)
        {
            try
            {
                var command = new CreateHospitalCommand() { Hospital = hospital, IsClientCommand = true };
                command.Execute();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool EditHospital(int oldHospitalID, HospitalModel newHospital)
        {
            try
            {
                var hos = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == oldHospitalID);

                if (hos != null)
                {
                    var command = new EditHospitalCommand() { OldHospital = hos, NewHospital = newHospital, IsClientCommand = true };
                    command.Execute();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool DeleteHospital(int hospitalID)
        {
            try
            {
                var hos = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == hospitalID);

                if (hos != null)
                {
                    var command = new DeleteHospitalCommand() { Hospital = hos, IsClientCommand = true };
                    command.Execute();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public IEnumerable<HospitalModel> GetAll()
        {
            return HospitalDatabase.DbContext.HospitalModels.Where(x => !x.IsDeleted).ToList();
        }

        public HospitalModel GetHospital(string hospitalName)
        {
            var hos = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.HospitalName == hospitalName);
            if (hos == null)
                return new HospitalModel();
            return hos;
        }

        public HospitalModel GetHospitalByID(int id)
        {
            var hos = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == id);
            if (hos == null)
                return new HospitalModel();
            return hos;
        }

        #endregion
    }
}
