﻿using Common.Interfaces;
using Hospital.Common.Models;
using Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class DepartmentServerProvider : IDepartment
    {
        public bool CreateOrUpdateDepartment(DepartmentModel department)
        {
            try
            {
                //if (!dbContext.Departments.Contains(department))
                //{
                //    // Create
                //    dbContext.Add(department);
                //    dbContext.SaveChanges();
                //}
                //else
                //{
                //    // Update
                //    var oldDepartment = dbContext.Departments.Find(department.ID);
                //    oldDepartment = department;
                //    dbContext.SaveChanges();
                //}
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public bool DeleteDepartment(int doctorID)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<DepartmentModel> GetAll(int hospitalID)
        {
            var departmentHospitals = HospitalDatabase.DbContext.DepartmentHospitals.Where(x => !x.IsDeleted && x.HospitalID == hospitalID).ToList();
            var allDepartments = HospitalDatabase.DbContext.DepartmentModels.Where(x => !x.IsDeleted).ToList();
            
            var departments = allDepartments
                                    .Where(a => departmentHospitals.Any(b => a.ID == b.DepartmentID))
                                    .Select(a => new DepartmentModel()
                                    {
                                        ID = a.ID,
                                        DepartmentName = a.DepartmentName,
                                        IsDeleted = a.IsDeleted
                                    }).ToList();




            return departments;
        }

        public DepartmentModel GetDepartment(string departmentName)
        {
            //return dbContext.Departments.SingleOrDefault(x => x.DepartmentName == departmentName);
            throw new NotImplementedException();
        }

        public DepartmentModel GetDepartmentByID(int id)
        {
            var model = HospitalDatabase.DbContext.DepartmentModels.SingleOrDefault(x => x.ID == id);
            if (model != null)
                return model;

            return new DepartmentModel();
        }
    }
}
