﻿using Common.Interfaces;
using Hospital.Common.Models;
using Server.Command;
using Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Server.Services
{
    public class DoctorServerProvider : IDoctor
    {

        #region Fields

        private static DoctorServerProvider instance;

        #endregion

        #region Properties

        public static DoctorServerProvider Instance
        {
            get
            {
                if (instance == null)
                    instance = new DoctorServerProvider();

                return instance;
            }
        }

        #endregion

        #region Constructors

        public DoctorServerProvider() { }

        #endregion

        public bool CreateDoctor(DoctorModel doctor)
        {
            try
            {
                var command = new CreateDoctorCommand() { Doctor = doctor, IsClientCommand = true };
                command.Execute();
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public IEnumerable<DoctorModel> GetAll(int hospitalID)
        {
            var doctorHospitals = HospitalDatabase.DbContext.DoctorHospitals.Where(x => !x.IsDeleted && x.HospitalID == hospitalID).ToList();
            var allDoctors = HospitalDatabase.DbContext.DoctorModels.Where(x => !x.IsDeleted).ToList();

            var doctors = allDoctors
                                    .Where(a => doctorHospitals.Any(b => a.ID == b.DoctorID))
                                    .Select(a => new DoctorModel()
                                    {
                                        ID = a.ID,
                                        FirstName = a.FirstName,
                                        LastName = a.LastName,
                                        DepartmentID = a.DepartmentID,
                                        DepartmentName = a.DepartmentName,
                                        Specialty = a.Specialty,
                                        Degree = a.Degree,
                                        IsDeleted = a.IsDeleted
                                    }).ToList();

            return doctors;
        }

        public DoctorModel GetDoctorByID(int id)
        {
            var doc = HospitalDatabase.DbContext.DoctorModels.SingleOrDefault(x => x.ID == id);
            if (doc == null)
                return new DoctorModel();
            return doc;
        }

        public bool DeleteDoctor(int doctorID)
        {
            try
            {
                var doc = HospitalDatabase.DbContext.DoctorModels.SingleOrDefault(x => x.ID == doctorID);

                if (doc != null)
                {
                    var command = new DeleteDoctorCommand() { Doctor = doc, IsClientCommand = true };
                    command.Execute();
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public bool EditDoctor(int oldDoctorID, DoctorModel newDoctor)
        {
            try
            {
                var doc = HospitalDatabase.DbContext.DoctorModels.SingleOrDefault(x => x.ID == oldDoctorID);

                if (doc != null)
                {
                    var command = new EditDoctorCommand() { OldDoctor = doc, NewDoctor = newDoctor, IsClientCommand = true };
                    command.Execute();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
