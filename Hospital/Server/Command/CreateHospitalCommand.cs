﻿using Hospital.Common.Models;
using Server.Database;
using System.Linq;

namespace Server.Command
{
    public class CreateHospitalCommand : Command
    {
        public HospitalModel Hospital { get; set; }
        public bool IsClientCommand { get; set; }

        public override void Execute()
        {
            Hospital.IsDeleted = false;
            HospitalDatabase.DbContext.HospitalModels.Add(Hospital);
            HospitalDatabase.DbContext.SaveChanges();
            Hospital.ID = HospitalDatabase.DbContext.HospitalModels.OrderByDescending(x => x.ID).FirstOrDefault().ID;

            if (IsClientCommand)
                CommandHandler.RedoHistory.Clear();
            else
                if (CommandHandler.RedoHistory.Count > 0)
                CommandHandler.RedoHistory.Remove(this);

            CommandHandler.UndoHistory.Add(this);

        }

        public override void Unexecute()
        {
            var hos = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == Hospital.ID);

            if (hos != null)
            {
                hos.IsDeleted = true;
                Hospital = hos;

                foreach (var item in HospitalDatabase.DbContext.DoctorHospitals.Where(x => x.HospitalID == Hospital.ID).ToList())
                {
                    item.IsDeleted = true;
                }
                foreach (var item in HospitalDatabase.DbContext.PatientHospitals.Where(x => x.HospitalID == Hospital.ID).ToList())
                {
                    item.IsDeleted = true;
                }
                foreach (var item in HospitalDatabase.DbContext.DepartmentHospitals.Where(x => x.HospitalID == Hospital.ID).ToList())
                {
                    item.IsDeleted = true;
                }

                HospitalDatabase.DbContext.SaveChanges();

                if (CommandHandler.UndoHistory.Count > 0)
                    CommandHandler.UndoHistory.Remove(this);

                CommandHandler.RedoHistory.Add(this);
            }
        }

        public CreateHospitalCommand()
        {
            ID = 1;
        }
    }
}
