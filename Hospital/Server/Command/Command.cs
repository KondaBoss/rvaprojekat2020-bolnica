﻿namespace Server.Command
{
    public abstract class Command
    {
        protected int ID;

        public Command()
        {

        }
        public abstract void Execute();

        public abstract void Unexecute();

    }
}
