﻿using Hospital.Common.Models;
using Server.Database;
using System.Linq;

namespace Server.Command
{
    public class EditHospitalCommand : Command
    {
        public HospitalModel OldHospital { get; set; }
        public HospitalModel NewHospital { get; set; }
        public bool IsClientCommand { get; set; }

        public override void Execute()
        {
            OldHospital.IsDeleted = false;
            HospitalModel temp = new HospitalModel()
            {
                ID = OldHospital.ID,
                HospitalName = OldHospital.HospitalName,
                HospitalType = OldHospital.HospitalType,
                NumberOfDepartments = OldHospital.NumberOfDepartments,
                NumberOfDoctors = OldHospital.NumberOfDoctors,
                IsDeleted = OldHospital.IsDeleted
            };

            NewHospital.IsDeleted = false;

            var hos = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == NewHospital.ID);

            if (hos != null)
            {
                hos.ID = NewHospital.ID;
                hos.HospitalName = NewHospital.HospitalName;
                hos.HospitalType = NewHospital.HospitalType;
                hos.NumberOfDepartments = NewHospital.NumberOfDepartments;
                hos.NumberOfDoctors = NewHospital.NumberOfDoctors;
                hos.IsDeleted = NewHospital.IsDeleted;

                HospitalDatabase.DbContext.SaveChanges();

                OldHospital = temp;

                if (IsClientCommand)
                    CommandHandler.RedoHistory.Clear();
                else
                if (CommandHandler.RedoHistory.Count > 0)
                    CommandHandler.RedoHistory.Remove(this);

                CommandHandler.UndoHistory.Add(this);
            }
        }

        public override void Unexecute()
        {
            NewHospital.IsDeleted = false;
            HospitalModel temp = new HospitalModel()
            {
                ID = NewHospital.ID,
                HospitalName = NewHospital.HospitalName,
                HospitalType = NewHospital.HospitalType,
                NumberOfDepartments = NewHospital.NumberOfDepartments,
                NumberOfDoctors = NewHospital.NumberOfDoctors,
                IsDeleted = NewHospital.IsDeleted
            };

            OldHospital.IsDeleted = false;

            var hos = HospitalDatabase.DbContext.HospitalModels.SingleOrDefault(x => x.ID == OldHospital.ID);

            if (hos != null)
            {
                hos.ID = OldHospital.ID;
                hos.HospitalName = OldHospital.HospitalName;
                hos.HospitalType = OldHospital.HospitalType;
                hos.NumberOfDepartments = OldHospital.NumberOfDepartments;
                hos.NumberOfDoctors = OldHospital.NumberOfDoctors;
                hos.IsDeleted = OldHospital.IsDeleted;

                HospitalDatabase.DbContext.SaveChanges();

                NewHospital = temp;

                if (CommandHandler.UndoHistory.Count > 0)
                    CommandHandler.UndoHistory.Remove(this);

                CommandHandler.RedoHistory.Add(this);
            }
        }

        public EditHospitalCommand()
        {
            ID = 3;
        }
    }
}
