﻿using Hospital.Common.Models;
using Server.Database;
using System.Collections.Generic;
using System.Linq;

namespace Server.Command
{
    public class DeleteHospitalCommand : Command
    {
        public HospitalModel Hospital { get; set; }
        public List<DoctorHospital> DoctorHospitals { get; set; }
        public List<PatientHospital> PatientHospitals { get; set; }
        public List<DepartmentHospital> DepartmentHospitals { get; set; }
        public bool IsClientCommand { get; set; }

        public override void Execute()
        {
            Hospital.IsDeleted = true;

            foreach (var item in HospitalDatabase.DbContext.DoctorHospitals.Where(x => x.HospitalID == Hospital.ID).ToList())
            {
                //DoctorHospitals.Add(new DoctorHospital()
                //{
                //    ID = item.ID,
                //    HospitalID = item.HospitalID,
                //    DoctorID = item.DoctorID,
                //    IsDeleted = false
                //});

                item.IsDeleted = true;
                DoctorHospitals.Add(item);
            }
            foreach (var item in HospitalDatabase.DbContext.PatientHospitals.Where(x => x.HospitalID == Hospital.ID).ToList())
            {
                item.IsDeleted = true;
                PatientHospitals.Add(item);
            }
            foreach (var item in HospitalDatabase.DbContext.DepartmentHospitals.Where(x => x.HospitalID == Hospital.ID).ToList())
            {
                item.IsDeleted = true;
                DepartmentHospitals.Add(item);
            }

            HospitalDatabase.DbContext.SaveChanges();

            if (IsClientCommand)
                CommandHandler.RedoHistory.Clear();
            else
                if (CommandHandler.RedoHistory.Count > 0)
                CommandHandler.RedoHistory.Remove(this);

            CommandHandler.UndoHistory.Add(this);
        }

        public override void Unexecute()
        {
            Hospital.IsDeleted = false;
            HospitalDatabase.DbContext.HospitalModels.Add(Hospital);

            HospitalDatabase.DbContext.SaveChanges();
            Hospital.ID = HospitalDatabase.DbContext.HospitalModels.OrderByDescending(x => x.ID).FirstOrDefault().ID;

            foreach (var item in DoctorHospitals)
            {
                item.IsDeleted = false;
                item.HospitalID = Hospital.ID;
                HospitalDatabase.DbContext.DoctorHospitals.Add(item);
            }

            foreach (var item in PatientHospitals)
            {
                item.IsDeleted = false;
                item.HospitalID = Hospital.ID;
                HospitalDatabase.DbContext.PatientHospitals.Add(item);
            }

            foreach (var item in DepartmentHospitals)
            {
                item.IsDeleted = false;
                item.HospitalID = Hospital.ID;
                HospitalDatabase.DbContext.DepartmentHospitals.Add(item);
            }

            HospitalDatabase.DbContext.SaveChanges();

            if (CommandHandler.UndoHistory.Count > 0)
                CommandHandler.UndoHistory.Remove(this);

            CommandHandler.RedoHistory.Add(this);
        }

        public DeleteHospitalCommand()
        {
            ID = 2;
            Hospital = new HospitalModel();
            DoctorHospitals = new List<DoctorHospital>();
            PatientHospitals = new List<PatientHospital>();
            DepartmentHospitals = new List<DepartmentHospital>();
        }
    }
}
