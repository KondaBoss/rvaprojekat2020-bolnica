﻿using Hospital.Common.Models;
using Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Command
{
    public class DeleteDoctorCommand : Command
    {
        public DoctorModel Doctor { get; set; }
        public bool IsClientCommand { get; set; }

        public override void Execute()
        {
            Doctor.IsDeleted = true;

            foreach (var item in HospitalDatabase.DbContext.DoctorHospitals.Where(x => x.DoctorID == Doctor.ID).ToList())
            {
                item.IsDeleted = true;
            }

            HospitalDatabase.DbContext.SaveChanges();

            if (IsClientCommand)
                CommandHandler.RedoHistory.Clear();
            else
                if (CommandHandler.RedoHistory.Count > 0)
                CommandHandler.RedoHistory.Remove(this);

            CommandHandler.UndoHistory.Add(this);
        }

        public override void Unexecute()
        {
            Doctor.IsDeleted = false;
            HospitalDatabase.DbContext.DoctorModels.Add(Doctor);
            HospitalDatabase.DbContext.SaveChanges();
            Doctor.ID = HospitalDatabase.DbContext.HospitalModels.OrderByDescending(x => x.ID).FirstOrDefault().ID;

            if (CommandHandler.UndoHistory.Count > 0)
                CommandHandler.UndoHistory.Remove(this);

            CommandHandler.RedoHistory.Add(this);
        }

        public DeleteDoctorCommand()
        {
            ID = 2;
        }
    }
}
