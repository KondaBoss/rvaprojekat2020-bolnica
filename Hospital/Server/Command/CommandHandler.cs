﻿using Common.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Server.Command
{
    public class CommandHandler : ICommandHandler
    {
        #region Fields

        private static CommandHandler instance;
        private static List<Command> undoHistory;
        private static List<Command> redoHistory;

        #endregion

        #region Properties
        public static CommandHandler Instance
        {
            get
            {
                if (instance == null)
                    instance = new CommandHandler();

                return instance;
            }
        }

        public static List<Command> UndoHistory
        {
            get
            {
                if (undoHistory == null)
                    undoHistory = new List<Command>();

                return undoHistory;
            }
        }

        public static List<Command> RedoHistory
        {
            get
            {
                if (redoHistory == null)
                    redoHistory = new List<Command>();

                return redoHistory;
            }
        }

        #endregion

        #region Constructors

        public CommandHandler()
        {

        }

        #endregion

        #region Methods

        public void Redo()
        {
            RedoHistory[RedoHistory.Count - 1].Execute();
        }

        public void Undo()
        {
            UndoHistory[UndoHistory.Count - 1].Unexecute();

        }

        public bool CanRedo()
        {
            if (RedoHistory.Any())
                return true;
            return false;
        }

        public bool CanUndo()
        {
            if (UndoHistory.Any())
                return true;
            return false;
        }

        #endregion

    }
}
