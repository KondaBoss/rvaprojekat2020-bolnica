﻿using Hospital.Common.Models;
using Server.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Server.Command
{
    public class EditDoctorCommand : Command
    {
        public DoctorModel OldDoctor { get; set; }
        public DoctorModel NewDoctor { get; set; }
        public bool IsClientCommand { get; set; }

        public override void Execute()
        {
            OldDoctor.IsDeleted = false;

            DoctorModel temp = new DoctorModel()
            {
                ID = OldDoctor.ID,
                FirstName = OldDoctor.FirstName,
                LastName = OldDoctor.LastName,
                Degree = OldDoctor.Degree,
                Specialty = OldDoctor.Specialty,
                DepartmentID = OldDoctor.DepartmentID,
                DepartmentName = OldDoctor.DepartmentName,
                IsDeleted = OldDoctor.IsDeleted
            };

            NewDoctor.IsDeleted = false;

            var doc = HospitalDatabase.DbContext.DoctorModels.SingleOrDefault(x => x.ID == NewDoctor.ID);

            if (doc != null)
            {
                doc.ID = NewDoctor.ID;
                doc.FirstName = NewDoctor.FirstName;
                doc.LastName = NewDoctor.LastName;
                doc.Degree = NewDoctor.Degree;
                doc.Specialty = NewDoctor.Specialty;
                doc.DepartmentID = NewDoctor.DepartmentID;
                doc.DepartmentName = NewDoctor.DepartmentName;
                doc.IsDeleted = NewDoctor.IsDeleted;

                HospitalDatabase.DbContext.SaveChanges();

                OldDoctor = temp;

                if (IsClientCommand)
                    CommandHandler.RedoHistory.Clear();
                else
                if (CommandHandler.RedoHistory.Count > 0)
                    CommandHandler.RedoHistory.Remove(this);

                CommandHandler.UndoHistory.Add(this);
            }
        }

        public override void Unexecute()
        {
            NewDoctor.IsDeleted = false;
            DoctorModel temp = new DoctorModel()
            {
                ID = NewDoctor.ID,
                FirstName = NewDoctor.FirstName,
                LastName = NewDoctor.LastName,
                Degree = NewDoctor.Degree,
                Specialty = NewDoctor.Specialty,
                DepartmentID = NewDoctor.DepartmentID,
                DepartmentName = NewDoctor.DepartmentName,
                IsDeleted = NewDoctor.IsDeleted
            };

            OldDoctor.IsDeleted = false;

            var doc = HospitalDatabase.DbContext.DoctorModels.SingleOrDefault(x => x.ID == NewDoctor.ID);

            if (doc != null)
            {
                doc.ID = OldDoctor.ID;
                doc.FirstName = OldDoctor.FirstName;
                doc.LastName = OldDoctor.LastName;
                doc.Degree = OldDoctor.Degree;
                doc.Specialty = OldDoctor.Specialty;
                doc.DepartmentID = OldDoctor.DepartmentID;
                doc.DepartmentName = OldDoctor.DepartmentName;
                doc.IsDeleted = OldDoctor.IsDeleted;

                HospitalDatabase.DbContext.SaveChanges();

                NewDoctor = temp;

                if (CommandHandler.UndoHistory.Count > 0)
                    CommandHandler.UndoHistory.Remove(this);

                CommandHandler.RedoHistory.Add(this);
            }
        }

        public EditDoctorCommand()
        {
            ID = 3;
        }
    }
}
