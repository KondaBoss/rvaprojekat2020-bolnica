﻿using Server.Services;
using System;

namespace Server
{
    class Program
    {
        static void Main(string[] args)
        {
            #region InitializeData

            DataInitializer.Instance.InitializeData();

            #endregion

            #region WCFService

            WCFServer _service = new WCFServer();
            _service.Open();

            #endregion

            Console.WriteLine("Press enter to close connection.");
            Console.ReadLine();

            _service.Close();

            Console.ReadLine();
        }
    }
}
