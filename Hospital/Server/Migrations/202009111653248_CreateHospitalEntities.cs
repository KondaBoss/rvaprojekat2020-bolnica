namespace Server.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateHospitalEntities : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DepartmentHospitals",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    DepartmentID = c.Int(nullable: false),
                    HospitalID = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.DepartmentModels",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    DepartmentName = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.DoctorHospitals",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    DoctorID = c.Int(nullable: false),
                    HospitalID = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.DoctorModels",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    FirstName = c.String(),
                    LastName = c.String(),
                    DepartmentID = c.Int(nullable: false),
                    DepartmentName = c.String(),
                    Specialty = c.Int(nullable: false),
                    Degree = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.HospitalModels",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    HospitalName = c.String(),
                    HospitalType = c.Int(nullable: false),
                    NumberOfDoctors = c.Int(nullable: false),
                    NumberOfDepartments = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.PatientHospitals",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    PatientID = c.Int(nullable: false),
                    HospitalID = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.PatientModels",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    JMBG = c.String(),
                    FirstName = c.String(),
                    LastName = c.String(),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

            CreateTable(
                "dbo.UserModels",
                c => new
                {
                    ID = c.Int(nullable: false, identity: true),
                    UserName = c.String(),
                    Password = c.String(),
                    FirstName = c.String(),
                    LastName = c.String(),
                    Role = c.Int(nullable: false),
                    IsDeleted = c.Boolean(nullable: false),
                })
                .PrimaryKey(t => t.ID);

        }

        public override void Down()
        {
            DropTable("dbo.UserModels");
            DropTable("dbo.PatientModels");
            DropTable("dbo.PatientHospitals");
            DropTable("dbo.HospitalModels");
            DropTable("dbo.DoctorModels");
            DropTable("dbo.DoctorHospitals");
            DropTable("dbo.DepartmentModels");
            DropTable("dbo.DepartmentHospitals");
        }
    }
}
