﻿using Hospital.Common.Models;
using System.Data.Entity;

namespace Server.Database
{
    public class PatientHospital
    {
        public int ID { get; set; }
        public int PatientID { get; set; }
        public int HospitalID { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class DoctorHospital
    {
        public int ID { get; set; }
        public int DoctorID { get; set; }
        public int HospitalID { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class DepartmentHospital
    {
        public int ID { get; set; }
        public int DepartmentID { get; set; }
        public int HospitalID { get; set; }
        public bool IsDeleted { get; set; }
    }

    public class HospitalDbContext : DbContext
    {
        public DbSet<UserModel> UserModels { get; set; }
        public DbSet<DepartmentModel> DepartmentModels { get; set; }
        public DbSet<HospitalModel> HospitalModels { get; set; }
        public DbSet<DoctorModel> DoctorModels { get; set; }
        public DbSet<PatientModel> PatientModels { get; set; }
        public DbSet<PatientHospital> PatientHospitals { get; set; }
        public DbSet<DoctorHospital> DoctorHospitals { get; set; }
        public DbSet<DepartmentHospital> DepartmentHospitals { get; set; }
    }
}
