﻿namespace Server.Database
{
    public class HospitalDatabase
    {
        #region Fields

        private static HospitalDbContext _dbContext;

        #endregion

        #region Properties
        //Singleton
        public static HospitalDbContext DbContext
        {
            get
            {
                if (_dbContext == null)
                    _dbContext = new HospitalDbContext();
                return _dbContext;
            }

        }

        #endregion

        #region Constructors

        public HospitalDatabase() { }

        #endregion
    }
}
