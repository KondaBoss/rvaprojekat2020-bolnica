﻿namespace Hospital.Common.Models
{
    public class PatientModel
    {
        public int ID { get; set; }

        public string JMBG { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool IsDeleted { get; set; }

        // One and only one Hospital
    }
}
