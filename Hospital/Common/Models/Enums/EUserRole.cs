﻿namespace Hospital.Common.Models.Enums
{
    public enum EUserRole
    {
        Regular,
        Administrator
    }
}
