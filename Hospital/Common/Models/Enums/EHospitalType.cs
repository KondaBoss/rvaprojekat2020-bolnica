﻿namespace Hospital.Common.Models.Enums
{
    public enum EHospitalType
    {
        Hospital,
        District,
        Specialized,
        Teaching,
        Clinic
    }
}
