﻿using Hospital.Common.Models.Enums;

namespace Hospital.Common.Models
{
    public class DoctorModel
    {
        public int ID { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public int DepartmentID { get; set; }

        public string DepartmentName { get; set; }

        public EMedicalSpecialty Specialty { get; set; }

        public EDoctorDegree Degree { get; set; }
        public bool IsDeleted { get; set; }

        // List of hospitals
    }
}
