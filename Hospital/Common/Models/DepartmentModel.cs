﻿namespace Hospital.Common.Models
{
    public class DepartmentModel
    {
        public int ID { get; set; }
        public string DepartmentName { get; set; }
        public bool IsDeleted { get; set; }
    }
}
