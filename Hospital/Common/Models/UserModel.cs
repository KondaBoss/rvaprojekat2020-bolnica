﻿using Hospital.Common.Models.Enums;

namespace Hospital.Common.Models
{
    public class UserModel
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public EUserRole Role { get; set; }
        public bool IsDeleted { get; set; }
    }
}
