﻿using Hospital.Common.Models.Enums;

namespace Hospital.Common.Models
{
    public class HospitalModel
    {
        public int ID { get; set; }

        public string HospitalName { get; set; }

        public EHospitalType HospitalType { get; set; }

        public int NumberOfDoctors { get; set; }

        public int NumberOfDepartments { get; set; }

        public bool IsDeleted { get; set; }

        // List of patients

        // List of doctors

        // List of departments
    }
}
