﻿using Hospital.Common.Models;
using System.Collections.Generic;
using System.ServiceModel;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IDoctor
    {
        [OperationContract]
        DoctorModel GetDoctorByID(int id);

        [OperationContract]
        bool CreateDoctor(DoctorModel doctor);

        [OperationContract]
        bool DeleteDoctor(int doctorID);

        [OperationContract]
        bool EditDoctor(int oldDoctorID, DoctorModel newDoctor);

        [OperationContract]
        IEnumerable<DoctorModel> GetAll(int hospitalID);
    }
}
