﻿using Hospital.Common.Models;
using System.Collections.Generic;
using System.ServiceModel;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IDepartment
    {
        [OperationContract]
        DepartmentModel GetDepartment(string departmentName);

        [OperationContract]
        DepartmentModel GetDepartmentByID(int id);

        [OperationContract]
        bool CreateOrUpdateDepartment(DepartmentModel department);

        [OperationContract]
        bool DeleteDepartment(int doctorID);

        [OperationContract]
        IEnumerable<DepartmentModel> GetAll(int hospitalID);
    }
}
