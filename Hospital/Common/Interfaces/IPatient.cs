﻿using Hospital.Common.Models;
using System.Collections.Generic;
using System.ServiceModel;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IPatient
    {
        [OperationContract]
        PatientModel GetPatient(string jmbg);

        [OperationContract]
        PatientModel GetPatientByID(int id);

        [OperationContract]
        bool DeletePatient(int patientID);

        [OperationContract]
        IEnumerable<PatientModel> GetAll(int hospitalID);

        [OperationContract]
        bool CreateOrUpdatePatient(PatientModel patient);
    }
}
