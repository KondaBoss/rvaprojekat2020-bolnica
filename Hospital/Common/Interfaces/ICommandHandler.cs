﻿using System.ServiceModel;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface ICommandHandler
    {
        [OperationContract]
        void Undo();

        [OperationContract]
        void Redo();

        [OperationContract]
        bool CanUndo();

        [OperationContract]
        bool CanRedo();
    }
}
