﻿using Hospital.Common.Models;
using System.ServiceModel;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IUser
    {
        [OperationContract]
        UserModel GetUser(string userName);

        [OperationContract]
        UserModel GetUserByID(int id);

        [OperationContract]
        bool CreateOrUpdateUser(UserModel user);
    }
}
