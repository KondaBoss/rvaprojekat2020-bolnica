﻿using Hospital.Common.Models;
using System.Collections.Generic;
using System.ServiceModel;

namespace Common.Interfaces
{
    [ServiceContract]
    public interface IHospital
    {
        [OperationContract]
        HospitalModel GetHospital(string hospitalName);

        [OperationContract]
        HospitalModel GetHospitalByID(int id);

        [OperationContract]
        bool CreateHospital(HospitalModel hospital);

        [OperationContract]
        bool EditHospital(int oldHospitalID, HospitalModel newHospital);

        [OperationContract]
        bool DeleteHospital(int hospitalID);

        [OperationContract]
        IEnumerable<HospitalModel> GetAll();
    }
}
